<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Productos;

/**
 * ProductosSearch represents the model behind the search form about `common\models\Productos`.
 */
class ProductosSearch extends Productos {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['id', 'activo', 'imagen', 'orden',  ], 'integer'],
                [['nombre', 'descripcion', 'keywords','fecha_creacion','fecha_modificacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Productos::find()->orderBy(['fecha_creacion' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');

            return $dataProvider;
        }


        //Filtro por rango de fecha
        if (!empty($this->fecha_creacion)) {
            
            Yii::error("filter fecha");
            $fechas_explode = explode(" - ", $this->fecha_creacion);
            if (count($fechas_explode) == 2) {
                $fecha_1 = new \DateTime($fechas_explode[0]);
                $fecha_1->setTime(0, 0);
                $fecha_2 = new \DateTime($fechas_explode[1]);
                $fecha_2->setTime(23, 59);
                $query->andFilterWhere(['between', 'fecha_creacion', $fecha_1->getTimestamp(), $fecha_2->getTimestamp()]);
            }
        }



        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'activo' => $this->activo,
            'imagen' => $this->imagen,
            'orden' => $this->orden,
            //'fecha_creacion' => $this->fecha_creacion,
            'fecha_modificacion' => $this->fecha_modificacion,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
                ->andFilterWhere(['like', 'descripcion', $this->descripcion])
                ->andFilterWhere(['like', 'keywords', $this->keywords]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchFront($params) {
        $query = Productos::find()->where(['activo' => true])->orderBy(['fecha_creacion' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'activo' => $this->activo,
            'imagen' => $this->imagen,
            'orden' => $this->orden,
            'fecha_creacion' => $this->fecha_creacion,
            'fecha_modificacion' => $this->fecha_modificacion,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
                ->andFilterWhere(['like', 'descripcion', $this->descripcion])
                ->andFilterWhere(['like', 'keywords', $this->keywords]);

        return $dataProvider;
    }

    public function searchRelacionadas($model, $exclude = null) {


        $query = Productos::find()->where(['productos.activo' => 1]);

        if (!is_null($exclude))
            $query->andFilterWhere(['<>', 'productos.id', $exclude]);

        //$query->andFilterWhere(['like', 'notas_categorias_id', $model->notas_categorias_id]);
        $query->andFilterWhere(['or',
                ['like', 'nombre', $model->keywords],
                ['like', 'descripcion', $model->keywords],
                ['like', 'keywords', $model->keywords],
                //['notas_categorias_id' => $model->notas_categorias_id],
        ]);

        $query->limit(4);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => \Yii::$app->devicedetect->isMobile() ? 2 : 4,
            ],
        ]);

        return $dataProvider;
    }

}
