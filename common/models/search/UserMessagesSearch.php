<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserMessages;

/**
 * UserMessagesSearch represents the model behind the search form about `common\models\UserMessages`.
 */
class UserMessagesSearch extends UserMessages {

    public $username;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['id', 'user_from', 'user_to', 'visto'], 'integer'],
                [['message', 'username', 'fecha'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$user_to = null ,$user_from = null) {
        
        $query = UserMessagesSearch::find()->joinWith('userTo')->select(['user_messages.*', 'user.username as username'])->orderBy(['fecha' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        
        if (!is_null($this->fecha) && strpos($this->fecha, ' - ') !== false)
        {
            list($start_fecha, $end_fecha) = explode(' - ', $this->fecha); 

            $start_fecha = strtotime(str_replace('/', '-', $start_fecha).' 00:00:00');
            $end_fecha = strtotime(str_replace('/', '-', $end_fecha).' 23:59:59');
            $query->andFilterWhere(['between', 'fecha', $start_fecha, $end_fecha]);
        }
        
        
        if(!is_null($user_to)){
            
        $query->andFilterWhere([
            'user_to' => $user_to,
        ]);
            
        }
        if(!is_null($user_from)){
            
        $query->andFilterWhere([
            'user_from' => $user_from,
        ]);
            
        }        
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_from' => $this->user_from,
            'user_to' => $this->user_to,
            //'fecha' => $this->fecha,
            'visto' => $this->visto,
        ]);

        $query->andFilterWhere(['like', 'message', $this->message]);

        $query->andFilterWhere(['like', 'username', $this->username]);

        return $dataProvider;
    }

}
