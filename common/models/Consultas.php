<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "consultas".
 *
 * @property integer $id
 * @property string $remitente
 * @property string $destinatario
 * @property string $asunto
 * @property string $cuerpo
 * @property integer $fecha_creacion
 * @property integer $visto
 */
class Consultas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consultas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['remitente', 'destinatario', 'asunto', 'cuerpo', 'fecha_creacion', 'visto'], 'required'],
            [['cuerpo'], 'string'],
            [['fecha_creacion', 'visto'], 'integer'],
            [['remitente', 'destinatario'], 'string', 'max' => 128],
            [['asunto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'remitente' => 'Remitente',
            'destinatario' => 'Destinatario',
            'asunto' => 'Asunto',
            'cuerpo' => 'Cuerpo',
            'fecha_creacion' => 'Fecha Creacion',
            'visto' => 'Visto',
        ];
    }
}
