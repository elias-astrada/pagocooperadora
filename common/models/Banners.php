<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\Archivos;

/**
 * This is the model class for table "banners".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $html
 * @property integer $imagen
 * @property string $link
 * @property integer $activo
 *
 * @property BannersXZonas[] $bannersXZonas
 * @property BannersZonas[] $zonas
 */
class Banners extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'banners';
    }

   
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['nombre', 'link', 'activo'], 'required'],
                [['html'], 'string'],
                [['imagen', 'activo'], 'integer'],
                [['nombre', 'link'], 'string', 'max' => 255],
        ];
    }
    

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        

        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'html' => 'Html',
            'imagen' => 'Imagen',
            'link' => 'Link',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannersXZonas() {
        return $this->hasMany(BannersXZonas::className(), ['banners_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZonas() {
        return $this->hasMany(BannersZonas::className(), ['id' => 'zonas_id'])->viaTable('banners_x_zonas', ['banners_id' => 'id']);
    }

    public static function getArrayList($agregarvacio = true) {
        $array = ArrayHelper::map(Banners::find()->where(['activo' => 1])->all(), 'id', 'nombre');
        if ($agregarvacio)
            $array = ArrayHelper::merge(['' => ''], $array);
        return $array;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImagen0() {
        return $this->hasOne(Archivos::className(), ['id' => 'imagen']);
    }

    public function render() {

        if (!empty($this->html))
            return $this->html;
        else {
            $img = $this->imagen0;
            if (is_null($img))
                return '';
            return Html::a(Html::img($img->getUrl()), $this->link, ['target' => '_blank']);
        }
        
    }

}
