<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "noticias_multimedia".
 *
 * @property integer $noticia_id
 * @property integer $archivos_id
 *
 * @property Archivos $archivos
 * @property Noticias $noticia
 */
class NoticiasMultimedia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'noticias_multimedia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['noticia_id', 'archivos_id'], 'required'],
            [['noticia_id', 'archivos_id'], 'integer'],
            [['archivos_id'], 'exist', 'skipOnError' => true, 'targetClass' => Archivos::className(), 'targetAttribute' => ['archivos_id' => 'id']],
            [['noticia_id'], 'exist', 'skipOnError' => true, 'targetClass' => Noticias::className(), 'targetAttribute' => ['noticia_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'noticia_id' => 'Noticia ID',
            'archivos_id' => 'Archivos ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArchivos()
    {
        return $this->hasOne(Archivos::className(), ['id' => 'archivos_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoticia()
    {
        return $this->hasOne(Noticias::className(), ['id' => 'noticia_id']);
    }
}
