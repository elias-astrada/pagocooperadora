<?php

namespace common\utils;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\UploadedFile;
use common\models\Archivos;

/**
 * Helper Para Archivos
 *
 * @author PC03-MAXI
 */
class Archivo extends Model {

    public $db_id;
    public $filename;
    public $route;
    public $extension;
    public $hasError = false;
    public $errorReport = [];

    public function getFilenameSinExtension() {

        return pathinfo($this->filename, PATHINFO_FILENAME);
    }

    public function isUploadSuccess() {

        return !$this->hasError;
    }

    public static function getArchivo($filename, $ruta = '/galeria/') {

        $archivo = new static;
        $archivo->filename = $filename;
        $archivo->route = $ruta;
        return $archivo;
    }

    public static function createFromUrl($url, $extension, $ruta = '/galeria/', $savedb = true) {

        $archivo = new static;
        $filename = uniqid();
        $archivo->filename = $filename . '.' . $extension;
        $archivo->extension = $extension;
        $archivo->route = $ruta;
        $route_file = $ruta . $archivo->filename;

        if (!copy($url, Yii::getAlias('@uploads') . $route_file)) {
            $archivo->hasError = true;
            $archivo->errorReport = ['error' => 'Error: No se pudo guardar el archivo en el servidor.'];
            return $archivo;
        }
        if ($savedb) {
            if (!$archivo->savedb()) {
                $archivo->hasError = true;
                $archivo->errorReport = ['error' => 'Error: No se pudo guardar el archivo en la base de datos.'];

                return $archivo;
            }
        }

        return $archivo;
    }

    public function getFile() {
        $file = Yii::getAlias('@uploads') . $this->route . $this->filename;
        return $file;
    }

    public function getAbsoluteUrl() {

        return Yii::$app->urlManagerFront->createAbsoluteUrl('uploads' . $this->route . $this->filename);
    }

    public function getUrl($recorte = null) {

        if (!is_null($recorte)) {

            switch ($recorte) {
                case '16/9x2':
                    return Yii::getAlias('@uploadsUrl') . $this->route . $this->getFilenameSinExtension() . '@2.' . $this->extension;
                case '16/9x1':
                    return Yii::getAlias('@uploadsUrl') . $this->route . $this->getFilenameSinExtension() . '@1.' . $this->extension;
                case '4/3x1':
                    return Yii::getAlias('@uploadsUrl') . $this->route . $this->getFilenameSinExtension() . '@43.' . $this->extension;
                case 'landscape':
                    return Yii::getAlias('@uploadsUrl') . $this->route . $this->getFilenameSinExtension() . '@ls.' . $this->extension;
                default:
                    return Yii::getAlias('@uploadsUrl') . $this->route . $this->getRecorteSize($recorte) . '/' . $this->filename;
            }
        }




        return Yii::getAlias('@uploadsUrl') . $this->route . $this->filename;
    }

    public static function uploadArchivo($file_instance, $ruta = '/galeria/', $savedb = true) {

        $file_uploaded = UploadedFile::getInstanceByName($file_instance);
        $archivo = new static;

        if (is_null($file_uploaded)) {
            $archivo->hasError = true;
            $archivo->errorReport = ['error' => 'Error: El servidor no recivió ningun archivo'];
            return $archivo;
        }
        $filename = uniqid();

        $archivo->filename = $filename . '.' . $file_uploaded->extension;
        $archivo->extension = $file_uploaded->extension;
        $archivo->route = $ruta;
 


        if (!file_exists(Yii::getAlias('@uploads') . $archivo->route)) {
            if (!mkdir(Yii::getAlias('@uploads') . $archivo->route)) {
                $archivo->hasError = true;
                $archivo->errorReport = ['error' => 'Error: El directorio no existe y no pudo ser generado.'];
                return $archivo;
            }
        }


        $route_file = $ruta . $archivo->filename;

        if (!$file_uploaded->saveAs(Yii::getAlias('@uploads') . $route_file, true)) {
            $archivo->hasError = true;
            $archivo->errorReport = ['error' => 'Error: No se pudo guardar el archivo en el servidor.'];
            return $archivo;
        }

        if ($savedb) {
            if (!$archivo->savedb()) {
                $archivo->hasError = true;
                $archivo->errorReport = ['error' => 'Error: No se pudo guardar el archivo en la base de datos.'];

                return $archivo;
            }
        }


        return $archivo;
    }

    public static function uploadArchivos($file_instance, $ruta = '/galeria/', $savedb = true) {

        $file_uploaded_array = UploadedFile::getInstancesByName($file_instance);
        $archivos = array();
        $archivo = new static;


        if (!file_exists(Yii::getAlias('@uploads') . $ruta)) {
            if (!mkdir(Yii::getAlias('@uploads') . $ruta)) {
                $archivo->hasError = true;
                $archivo->errorReport = ['error' => 'Error: El directorio no existe y no pudo ser generado.'];
                return $archivo;
            }
        }

        foreach ($file_uploaded_array as $file_uploaded) {

            $archivo = new static;
            $archivos[] = $archivo;
            if (is_null($file_uploaded)) {
                $archivo->hasError = true;
                $archivo->errorReport = ['error' => 'Error: El servidor no recivió ningun archivo'];
                //return $archivo;
                continue;
            }
            $filename = uniqid();

            $archivo->filename = $filename . '.' . $file_uploaded->extension;
            $archivo->extension = $file_uploaded->extension;
            $archivo->route = $ruta;

            $route_file = $ruta . $archivo->filename;

            if (!$file_uploaded->saveAs(Yii::getAlias('@uploads') . $route_file, true)) {
                $archivo->hasError = true;
                $archivo->errorReport = ['error' => 'Error: No se pudo guardar el archivo en el servidor.'];
            }
            if ($savedb && !$archivo->hasError) {
                if (!$archivo->savedb()) {
                    $archivo->hasError = true;
                    $archivo->errorReport = ['error' => 'Error: No se pudo guardar el archivo en la base de datos.'];
                }
            }
        }

        return $archivos;
    }

    public function savedb() {

        if (is_null($this->db_id)) {
            $f = new Archivos();
            $f->alt = "";
        } else
            $f = Archivos::findOne($id);

        if (is_null($f)) {
            Yii::error('No existe el registro en db.');
            return false;
        }

        $f->ruta = $this->route;
        $f->nombre = $this->filename;
        $f->fecha_subida = time();
        $f->url = 0;
        $f->extension = $this->extension;
        $f->tipo = "file";

        if (!$f->save()) {
            Yii::error(var_export($f->errors, true));
            return false;
        }
        $this->db_id = $f->getPrimaryKey();
        return true;
    }

    public static function loaddb($id) {
        $f = Archivos::findOne($id);
        if (is_null($f)) {
            Yii::error('No existe el registro en db.');
            return null;
        }
        $archivo = new static;
        $archivo->filename = $f->nombre;
        $archivo->route = $f->ruta;
        $archivo->db_id = $id;
        $archivo->extension = $f->extension;
        return $archivo;
    }

    public function getDbRecord() {
        $f = Archivos::findOne($this->db_id);
        if (is_null($f)) {
            return null;
        }
        return $f;
    }

    public function delete() {
        $file = $this->getFile();
        if (is_file($file))
            unlink($file);
        if (!empty($this->db_id)) {
            $f_db = $this->getDbRecord();

            if (!is_null($f_db))
                $f_db->delete();
        }
    }

}
