<?php


use kartik\grid\GridView;

$defaultExportConfig = [
    GridView::HTML => [],
    GridView::CSV => [],
    GridView::TEXT => [],
    GridView::EXCEL => [],
    GridView::PDF => [],
    GridView::JSON => [],
];

return [
    'adminEmail' => 'admin@example.com',

        'gridview_exportConfig' => $defaultExportConfig,
];
