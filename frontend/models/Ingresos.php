<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ingresos".
 *
 * @property int $id
 * @property string $fecha
 * @property string $razon
 * @property string $importe
 * @property string $metodo
 */
class Ingresos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingresos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['razon', 'importe', 'metodo'], 'required'],
            [['fecha'], 'safe'],
            [['metodo'], 'string'],
            [['razon', 'importe'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'razon' => 'Razon',
            'importe' => 'Importe (en pesos)',
            'metodo' => 'Metodo',
        ];
    }
}
