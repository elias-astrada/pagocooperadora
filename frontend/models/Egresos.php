<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "egresos".
 *
 * @property int $id
 * @property string $fecha
 * @property string $razon
 * @property string $importe
 */
class Egresos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'egresos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['razon', 'importe'], 'required'],
            [['razon'], 'string', 'max' => 255],
            [['importe'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'razon' => 'Razon',
            'importe' => 'Importe',
        ];
    }
}
