<?php

namespace frontend\controllers;

use Yii;
use common\models\Productos;
use common\models\search\ProductosSearch;
use yii\web\NotFoundHttpException;

class ProductosController extends \yii\web\Controller {

    public function actionDetalle($id) {
        
        
        $model = $this->findModel($id);
        
        $searchModel = new ProductosSearch();
        $relacionados = $searchModel->searchRelacionadas($model, $id);
        
        
        
        return $this->render('detalle',[ 'model' => $model ,'relacionados' => $relacionados]);
    }

    public function actionIndex() {

        
        $search = Yii::$app->request->get('q');
        $searchModel = new ProductosSearch();
        $dataProvider = $searchModel->searchFront($search);
        


        return $this->render('index',['productos_dataprovider' => $dataProvider , 'search' => $search]);
    }

    /**
     * Finds the Productos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Productos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Productos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La página solicitada no existe.');
        }
    }

}
