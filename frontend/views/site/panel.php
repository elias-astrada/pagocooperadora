<body style="background-image:url('https://i.imgur.com/AXKJFmf.png');background-size:cover;">

<?php 

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Panel - Pago Cooperadora';
 ?>
<section class="jumbotron text-center bg-primary text-white">
        <div class="container">
            <h1 class = "jumbotron-heading"><i class="fa fa-home"></i> Panel principal</h1>
        </div>
    </section>
 <div class="row">
 	<div class="col-md-4">
 		<div class="card bg-success text-white p-2" style="border-radius: 0;">
 			<h4><i class="fa fa-hand-holding-usd"></i> Añadir ingresos</h4>
 		</div>
 		<a href="<?= Url::toRoute('ingresos/index')?>" class="btn btn-secondary btn-block" style="border-radius: 0;">Acceder</a>
 	</div>
 	<div class="col-md-4">
 		<div class="card bg-danger text-white p-2" style="border-radius: 0;">
 			<h4><i class="fa fa-minus"></i> <i class="fa fa-dollar-sign"></i> Añadir gastos</h4>
 		</div>
 		<a href="<?= Url::toRoute('egresos/index')?>" class="btn btn-secondary btn-block" style="border-radius: 0;">Acceder</a>
 	</div>
 	<div class="col-md-4">
 		<div class="card bg-primary text-white p-2" style="border-radius: 0;">
 			<h4><i class="fa fa-user-plus"></i> Añadir alumnos</h4>
 		</div>
 		<a href="<?= Url::toRoute('alumnos/index')?>" class="btn btn-secondary btn-block" style="border-radius: 0;">Acceder</a>
 	</div>
 </div>
 <br>
 <h2>Totales:</h2>
 <hr style="border-color: #000;">
 <br>
 <div class="row">
 	<div class="col-md-4">
 		<div class="card bg-success text-white p-2" style="border-radius: 0;">
 			<h4><i class="fa fa-hand-holding-usd"></i> Ingresos</h4>
 		</div>
 		<div class="card bg-dark text-white p-1" style="border-radius: 0;">

 			<ul>
 				<li><b>Ingresos:</b> <?= $ingresos ?> $</li>
 				<li><b>Ingresos por alumno:</b> <?= $ingresosalumnos ?> $</li>
 				<li><b>Total:</b> <?= $ingresos + $ingresosalumnos ?> $</li>
 			</ul>
 		</div>
 	</div>
 	<div class="col-md-4">
 		<div class="card bg-danger text-white p-2" style="border-radius: 0;">
 			<h4><i class="fa fa-minus"></i> <i class="fa fa-dollar-sign"></i> Gastos</h4>
 		</div>
 		<div class="card bg-dark text-white text-center" style="border-radius: 0;">
 			<h1><?= $gastos ?> $</h1>
 		</div>
 	</div>
 	<div class="col-md-4">
 		<div class="card bg-primary text-white p-2" style="border-radius: 0;">
 			<h4><i class="fa fa-check"></i> Total</h4>
 		</div>
 		<div class="card bg-dark text-white text-center" style="border-radius: 0;">
 			<h1><?= $ingresos + $ingresosalumnos - $gastos ?> $</h1>
 		</div>
 	</div>
 </div>