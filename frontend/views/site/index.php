<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Home';
$login_url = Url::toRoute(['/user/login']);
$register_url = Url::toRoute(['/user/register']);
?>
<div class="site-index">

  <section class="jumbotron text-center bg-primary text-white">
        <div class="container">
            <h1 class = "jumbotron-heading">Sistema de pago Cooperadora</h1>
            <p>Instituto: -</p>
        </div>
    </section>

    <div class="body-content">

      <div class="row">
           <div class="col-md-12">
            <?php
            Pjax::begin(['id' => 'login-pjax']);
            Pjax::end();
            $this->registerJs(' $.pjax.reload({container: "#login-pjax", replace:false, async:false,timeout: 2000, url: "' . $login_url . '"  });  ', \yii\web\View::POS_READY);
            ?>
        </div>
      </div>

    </div>
</div>
