<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Egresos */

$this->title = 'Crear Gasto';
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="egresos-crear">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>