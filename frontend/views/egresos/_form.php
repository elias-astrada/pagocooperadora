<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Egresos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="egresos-form">
	<br>
    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i><?= Html::encode($this->title) ?></h3></div>

        <div class="panel-body">

	    <?= $form->field($model, 'razon')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'importe')->textInput(['maxlength' => true]) ?>






        </div>

    </div>
    <div class="form-group text-center">
       <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
        <a href="<?= Url::toRoute('egresos/index') ?>" class="btn btn-primary btn-block">Regresar al listado de Gastos</a>
    </div>     

    <?php ActiveForm::end(); ?>

</div>
