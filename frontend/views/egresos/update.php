<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Egresos */

$this->title = 'Modificar Gasto: ' . $model->id;
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="egresos-modificar">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>


</div>