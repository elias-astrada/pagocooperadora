<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Alumnos */

$this->title = 'Añadir Alumnos';
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="alumnos-crear">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>