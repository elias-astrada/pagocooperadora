<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Alumnos */

$this->title = 'Modificar Alumno, ID: ' . $model->id;
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="alumnos-modificar">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>


</div>