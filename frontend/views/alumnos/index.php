<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel app\models\AlumnosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alumnos - PagoCoop';
?>
<div class="ingresos-index">



    <div class="jumbotron bg-primary text-white" style="border-radius: 0;">
          
    <h1><i class="fa fa-hand-holding-usd"></i> Listado de Alumnos abonados</h1>
          <a class="btn btn-primary" href="<?= Url::toRoute('site/panel') ?>"><i class="fa fa-home"></i> Regresar a inicio</a>
    </div>



   <!-- <h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php

    $toolbar =  Html::a('<i class="fa fa-plus"></i> Nuevo ', ['create'], ['class' => 'btn btn-success']). 
                Html::a('<i class="fa fa-trash"></i>', 'javascript:void(0)' , ['id' => 'btn-bulk-del', 'class' => 'btn btn-danger', 'title'=>'Borrar Seleccionados']) . 
                Html::a('<i class="fa fa-sync-alt"></i>', ['index'], ['class'=>'btn btn-primary', 'title'=>'Actualizar']);?>

    <?= GridView::widget([
        'showPageSummary' => true,
        'export' => [
            'target' => GridView::TARGET_SELF,
            'label' => 'Exportar',
            'class' => 'btn btn-primary',
        ],
        'exportConfig' => Yii::$app->params['gridview_exportConfig'],
        'dataProvider' => $dataProvider,
        'toolbar'=> [
                  ['content'=>  $toolbar],
                  '{export}',
                  '{toggleData}',
        ],
        'pjax' => true,
        'bordered' => false,
        'striped' => true,
        'condensed' => false,
        'panel'=>[
            'type'=>'bg-primary',
        ],
        'panelHeadingTemplate' => '
        <h3 class="panel-title">{heading}</h3>
        <div class="panel-actions panel-actions-keep">{toolbar}</div>
        <div class="clearfix"></div>',
        'panelBeforeTemplate' => '{before}',
        'containerOptions' => ['class' => 'grid-pjax-container'],
        'options' => ['id' => 'grid-pjax'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\CheckboxColumn'],
            //['class' => 'kartik\grid\SerialColumn'],

            
            'id',
            'nombres',
            'apellidos',
            'aula',
            [
                'attribute' => 'importe',
                'pageSummary' => true,
            ],

            
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{update}{delete}',
                'buttons' => [
                  'update' => function ($url, $model, $key) {
                      return Html::a('', $url, ['class' => 'btn btn-info fa fa-edit','target' => '_BLANK']);
                  },
                ],
                'updateOptions' => [
                    'class' => 'btn btn-primary  btn-lg',  
                ],
                'deleteOptions' => [
                    'class' => 'fa fa-trash btn btn-danger ',
                ],
                          
                'width' => '95px',
                'noWrap' => true,
                
            ], 
        ],
    ]); ?>
</div>


<div class="modal fade modal-scroll" id="ajax" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>



<?=    $this->registerJs(' 
    $(document).ready(function(){
      $("#btn-bulk-del").click(function(){
        $.post(
            "delete-multiple", 
            {
                pk : $("#grid-pjax").yiiGridView("getSelectedRows")
            },
            function () {
                $.pjax.reload({container:"#grid-pjax"});
            }
        );
      });
    });', \yii\web\View::POS_READY);
?>