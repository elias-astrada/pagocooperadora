<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = $model->nombre;

Yii::$app->view->params['title-seo'] = $model->nombre;
//Yii::$app->view->params['description-seo'] = "";
?>

<div class="col-lg-10">
    <div class="article-head">

        <h3><?= $model->nombre ?></h3>
        <img src="<?= $model->imagen0->getUrl("16/9x1") ?>"  alt="..." class="img-responsive">
        <p><?= $model->descripcion ?></p>
        <?= $this->render('/common/socials'); ?>
    </div><!-- /.article__head-inner -->


</div>



<div class="col-lg-2">

    <h4 class="others">Otros productos que te pueden interesar</h4>
    <?php if (count($relacionados->models) > 0): ?>
        <?= $this->render('_list_productos', ['dataProvider' => $relacionados]); ?>

    <?php endif; ?>

</div>
