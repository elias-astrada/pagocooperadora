<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ingresos */

$this->title = 'Modificar Ingreso, ID: ' . $model->id;
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="ingresos-modificar">
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>


</div>