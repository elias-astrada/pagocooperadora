<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Ingresos */

$this->title = 'Añadir Ingresos - PagoCoop';
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="ingresos-crear">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>