<?php

namespace backend\controllers;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Archivos;
use Yii;
use yii\helpers\Json;

class ImagenesController extends \yii\web\Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            /* 'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
              'delete' => ['POST'],
              'delete-multiple' => ['POST'],
              ],
              ], */

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['index', 'recortar', 'subir', 'borrar', 'input'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionInput() {
        $model = new Archivos();
        $dataProvider = $model->searchImagenes();

        return $this->renderAjax('input', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex() {
        //return $this->render('index');


        $model = new Archivos();
        $dataProvider = $model->searchImagenes();

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionBorrar($id) {
        $imagen = \common\utils\Imagen::loaddb($id);

        if (!is_null($imagen))
            $imagen->delete();
    }

    public function actionRecortar($id) {

        $imagen = \common\utils\Imagen::loaddb($id);
        $recortes = \common\utils\Imagen::getRecortes();
        if (Yii::$app->request->isAjax) {



            if (Yii::$app->request->isPost) {

                $recorte_selected = Yii::$app->request->post('recorte');
                $cropdata = Yii::$app->request->post('cropdata');

                $imagen->cropmanual($cropdata['width'], $cropdata['height'], $cropdata['x'], $cropdata['y'], $imagen->getFiletoRecorte($recorte_selected));
                return Json::encode([
                            'success' => true,
                ]);
            }

            return $this->renderAjax('recortar', ['id' => $id, 'model' => $imagen, 'recortes' => $recortes]);
        }

        return $this->render('recortar', ['id' => $id, 'model' => $imagen, 'recortes' => $recortes]);
    }

    public function actionSubir() {

        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->isPost) {

                $f_helper = \common\utils\Imagen::uploadArchivo('imagen-upload', "/galeria/", true);




                if (!$f_helper->hasError) {

                    $tamaño = getimagesize($f_helper->getFile());

                    if ($tamaño[0] < 1280 || $tamaño[1] < 720) {
                        return Json::encode([
                                    'error' => 'La imagen debe tener un tamaño mínimo de 1280x720',
                        ]);
                        $f_helper->delete();
                    }
                    $f_helper->generarRecortes();

                    return Json::encode([
                                'success' => true,
                                'uploadedid' => $f_helper->db_id,
                                'uploadedurl' => $f_helper->getUrl()
                    ]);
                } else {

                    return Json::encode($f_helper->errorReport);
                }
            }
            return $this->renderAjax('subir');
        }

        return $this->render('subir');
    }

}
