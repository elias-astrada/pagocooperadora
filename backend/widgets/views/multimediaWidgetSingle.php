<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\widgets\ActiveForm;
use yii\helpers\Url;

/*
  $input = Html::activeInput('hidden', $this->model, $this->attribute, ['class' => 'imgpanel-' . $this->attribute]);
 */
$nothumbUrl = Yii::$app->urlManagerFront->createUrl(['/uploads/nothumb.png']);
$removemultimediaurl = Url::toRoute(['/multimedia/delete']);
$cropper_url = Url::toRoute(['/multimedia/recortar']);

?> 
<div id="multimedia-container-widget-<?= $wid ?>">
    <input type="hidden" id="multimedia-<?= $attribute ?>-ids" name="<?= $model->formName()?>[<?= $attribute ?>]" value="<?= !empty($attrvalue = $model->getAttribute($attribute)) ? $attrvalue : '' ?>">
    <?= ""/* Html::activeInput('hidden', $this->model, $this->attribute, ['class' => 'multimedaw-' . $this->attribute]); */ ?>



    <div class="img-panel-widget multimedia-list-item multimedia-<?= $attribute ?>-widget" style="max-width:20rem " data-multimedia-id = "<?= $model->id ?>">


        <figure class="overlay overlay-hover">

            <img src="<?= !empty($previewUrl) ? $previewUrl : $nothumbUrl ?>" alt="..." class="img-panel-widget-preview img-rounded img-fluid" >
            <figcaption class="overlay-panel overlay-background overlay-slide-left overlay-icon">
                <a class="icon md-delete multimedia-delete-button-<?= $wid ?>" href="#" ></a>
                <a class="icon md-crop multimedia-crop-button remote-modal" target = '_BLANK'  data-target = "#modal-multimedia-recortes"  data-toggle = "modal" href="<?= Url::toRoute(['/multimedia/recortar', 'id' => $model->id]) ?>"></a>
                <a class="icon md-search multimedia-zoom-button" data-fancybox="gallery" href="<?=  !empty($previewUrl) ? $previewUrl : $nothumbUrl ?>" ></a>
            </figcaption>
        </figure> 


        <a href="javascript:void(0)"  class="btn btn-block btn-primary" data-toggle="modal" data-target="#modal-multimedia-select-<?= $wid ?> " >Elegir imagen</a>
    </div>



    <?= "" /* $this->render("multimediaModals", compact('dataProvider_galeria', 'wid')) */ ?>

</div>

<?php

$this->registerJs(
        <<<JAVASCRIPT
        
        
    $('a.md-search').magnificPopup({
      type: 'image'
      // other options
    });
        
    $(document).on("click", ".multimedia-delete-button-$wid", function() {
            
        var id_multimedia = $("#multimedia-$attribute-ids").val();
        

        $(this).closest("figure").find("img").attr("src","$nothumbUrl");
        $(this).closest("figcaption").find(".multimedia-zoom-button").attr("src","$nothumbUrl");
        
        remove_multimedia_$wid(id_multimedia );
    });
        
        
    function select_multimedia_$wid(url_img , archivo_id ,tipo ){
        
       
        switch(tipo){
            
            case 'vid':
                $('.img-panel-widget-preview').attr("src",url_img);

            break;
            case 'img': default:
                $('.img-panel-widget-preview').attr("src",url_img);
            break;
        
        }
        
        register_on_input_$wid(archivo_id);
        $('a.md-search').magnificPopup({
            type: 'image'
            // other options
        });
        
    }
        
        
    function remove_multimedia_$wid(archivo_id){
        
        

        unregister_on_input_$wid(archivo_id);
        
        
        
       /* $.ajax({
            type: "POST",
            url: "$removemultimediaurl",
            data: {multimedia_id : archivo_id},

            success: function(data) {
            },

        });*/
        
    }

    function register_on_input_$wid(multimedia_id){

        $("#multimedia-$attribute-ids").val(multimedia_id);
        
        var cropbutt = $(".multimedia-$attribute-widget").find(".multimedia-crop-button");
        cropbutt.attr("href","$cropper_url"+"?id="+multimedia_id);
        
        
        var zoombutt = $("")

    }  
        
        
    function unregister_on_input_$wid(multimedia_id){
        var inputvalue = $("#multimedia-$attribute-ids").val(" ");

        var cropbutt = $(".multimedia-$attribute-widget").find(".multimedia-crop-button");
        cropbutt.attr("href","$cropper_url");
        

    }

        
JAVASCRIPT
);
