<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model yii\web\IdentityInterface */

$this->title = Yii::t('rbac-admin', 'Assignments');
 /* 
$this->params['breadcrumbs'][] = $this->title;
$this->params['title-page'] = Html::encode($this->title);*/
?>

<?php $this->beginContent('@backend/views/user/admin/update.php', ['user' => $user]) ?>


<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-9',
        ],
    ],
]); ?>

    
        <div class="form-group">
            <label class="control-label col-md-3">Role</label>
            <div class="col-md-9">
                <select name="role" class="form-control select2 select2-offscreen" data-placeholder="Select..." tabindex="-1" >
                    <option value="">Without role</option>
                <?php if (!empty($avaliable['Roles'])): ?>                    
                    <?php foreach ($avaliable['Roles'] as $a): ?>
                        <option value="<?= $a ?>" <?= $assigned == $a ? 'selected="selected"' : '' ?> ><?= $a ?></option>
                    <?php endforeach ?>
                <?php endif ?>
                </select>
            </div>
        </div>
        <div class="form-actions fluid">
            
            <div class="col-md-offset-3 col-md-9">
                <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-success btn-block btn-sm']) ?>
            </div>
        </div>

<?php ActiveForm::end(); ?>

<?php $this->endContent() ?>