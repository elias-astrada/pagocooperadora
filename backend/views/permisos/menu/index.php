<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\Menu */

$this->title = Yii::t('rbac-admin', 'Menus');
$this->params['breadcrumbs'][] = $this->title;

$toolbar =  Html::a('<i class="glyphicon glyphicon-plus"></i> Nuevo ', ['create'], ['class' => 'btn btn-success']). 
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['class'=>'btn btn-default', 'title'=>'Actualizar']);
?>
<div class="menu-index">

    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'exportConfig' => Yii::$app->params['gridview_exportConfig'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'toolbar'=> [
                  ['content'=>  $toolbar],
                  '{export}',
                  '{toggleData}',
        ],
        'bordered' => false,
        'striped' => true,
        'condensed' => false,
        'pjax' => true,
        'panel'=>[
            'heading'=>'<i class="fa fa-table" aria-hidden="true"></i> '.Html::encode($this->title),
            'type'=>'primary',
            'footer'=>false,
        ],
        'panelHeadingTemplate' => '
        <h3 class="panel-title">{heading}</h3>
        <div class="panel-actions panel-actions-keep">{toolbar}</div>
        <div class="clearfix"></div>',
        'panelBeforeTemplate' => '{before}',
        'containerOptions' => ['class' => 'grid-pjax-container'],
        'options' => ['id' => 'grid-pjax'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'menuParent.name',
                'filter' => Html::activeTextInput($searchModel, 'parent_name', [
                    'class' => 'form-control', 'id' => null
                ]),
                'label' => Yii::t('rbac-admin', 'Parent'),
            ],
            'route',
            'order',
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => [
                  'view' => function ($url, $model, $key) {
                      return Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-info btn-action','target' => '_BLANK', 'data-target' => '#ajax', 'data-toggle' => 'modal']);
                  },
                ],
                'updateOptions' => [
                  'class' => 'btn btn-primary btn-action',  
                ],
                'deleteOptions' => [
                    'class' => 'btn btn-danger btn-action',
                ],
                          
                'width' => '95px',
                'noWrap' => true,
            ], 
        ],
    ]);
    ?>
<?php Pjax::end(); ?>

</div>



<div class="modal fade modal-scroll" id="ajax" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>
