<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Banners */

$this->title = 'Modificar Banners: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Banners', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Modificar';
Yii::$app->params['page-header-showtitle'] = false;
?>
<div class="banners-modificar">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>


</div>