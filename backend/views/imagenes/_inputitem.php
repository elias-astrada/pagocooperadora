<?php

use yii\helpers\Html;
use yii\helpers\Url;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * data-toggle="slidePanel" data-url="<?= Url::toRoute(['/imagenes/recortar', 'id' => $model->id]) ?>"
 */
?>



<a href="javascript:void(0)" class="img-input-select" data-imgid="<?= $model->id ?>" data-imgurl="<?= $model->getUrl() ?>">
    <img src="<?= $model->getUrl() ?>" class="image img-thumbnail img-rounded img-fluid" alt="..." >
</a>

<?php
$this->registerJs(
<<<JAVASCRIPT

  /* $('.btn-imagen-delete').bind("ajax-call-response",function(e){
        toastr["success"]("La imagen ha sido eliminada exitosamente!");
        $.pjax.reload({container: '#pjax-imagenes-container', timeout: 10000});
   })*/
        
JAVASCRIPT
);
