<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\ListView;
use nirvana\infinitescroll\InfiniteScrollPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\NoticiasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//\backend\assets\ImagenesAsset::register($this);

$this->title = 'Imágenes';

$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['page-header-show'] = false;
Yii::$app->params['page-content-show'] = false;
Yii::$app->params['page-body-class'] = 'app-media page-aside-left site-menubar-unfold';
?>

<!-- Media Content -->
<div class="page-main">
    <!-- Media Content Header -->
    <br>
    <!-- Media Content -->
    <div id="mediaContent" class="page-content page-content-table <?=  empty (Yii::$app->request->get('class')) ? '' : Yii::$app->request->get('class'); ?>" data-plugin="selectable">
        <!-- Actions -->
        <div class="page-content-actions">
            <h2 style="display:inline-block; margin-right: 2rem;">Selecciona una imagen</h2>
  
                <?= Html::a('<i class="front-icon md-upload animation-scale-up" aria-hidden="true"></i> Subir Nueva Imagen', ['/imagenes/subir'], ['class' => 'btn btn-success remote-modal', 'target' => '_BLANK', 'data-target' => '#modal-subir', 'data-toggle' => 'modal']); ?>



        </div>


        <!-- Media -->


        <div class="media-list is-grid pb-50" data-plugin="animateList" data-animate="fade" data-child="li">
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'id' => 'galeria-input-list',
                'layout' => '<div class="items">{items}</div>{pager}',
                //'layout' => '{items}<div class="pager">{pager}</div>',
                'itemOptions' => ['tag' => 'li', 'class' => 'galeria-input-item'],
                'options' => [
                    'tag' => 'ul',
                    'class' => '',
                /* 'data-plugin' => "animateList",
                  'data-child' => ">li", */
                ],
                'itemView' => '_inputitem',
                'pager' => [
                    'class' => InfiniteScrollPager::className(),
                    'widgetId' => 'galeria-input-list',
                    'itemsCssClass' => 'items',
                    'nextPageLabel' => 'Cargar más Imágenes',
                    'contentLoadedCallback' => 'function(data) { if(data.length < ' . $dataProvider->pagination->pageSize . ') $(".list-notas-actions").hide(); }',
                    'linkOptions' => [
                        'class' => 'btn btn-primary btn-loadmore',
                    ],
                    'pluginOptions' => [
                        'loading' => [
                            'msgText' => "<em>Cargando...</em>",
                            'finishedMsg' => "<em>No se han encontrado mas resultados</em>",
                        ],
                        'behavior' => InfiniteScrollPager::BEHAVIOR_TWITTER,
                    ],
                ],
            ]);
            ?>

        </div>

    </div>
</div>



