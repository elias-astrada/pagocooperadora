<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Productos */

$this->title = 'Crear Productos';
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['page-header-showtitle'] = false;

?>
<div class="productos-crear">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>


</div>