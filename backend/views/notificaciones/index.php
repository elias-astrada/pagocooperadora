<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use mdm\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notificaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notificaciones-index">

    <?php
    $toolbar = Html::a('<i class="glyphicon glyphicon-trash"></i>',  'javascript:void(0)', ['id' => 'btn-bulk-del', 'class' => 'btn btn-danger', 'title' => 'Borrar Seleccionados']) .
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['class' => 'btn btn-default', 'title' => 'Actualizar']);
    ?>

    <?=
    GridView::widget([
        'exportConfig' => Yii::$app->params['gridview_exportConfig'],
        'dataProvider' => $dataProvider,
        'toolbar' => [
            ['content' => $toolbar],
            '{toggleData}',
        ],
        'bordered' => false,
        'striped' => true,
        'condensed' => false,
        'pjax' => true,
        'panel' => [
            'heading' => '<i class="fa fa-table" aria-hidden="true"></i> ' . Html::encode($this->title) ,
            'type' => 'primary',
            'footer' => false,
        ],
        'panelHeadingTemplate' => '
        <h3 class="panel-title">{heading}</h3>
        <div class="panel-actions panel-actions-keep">{toolbar}</div>
        <div class="clearfix"></div>',
        'panelBeforeTemplate' => '{before}',
        'containerOptions' => ['class' => 'grid-pjax-container'],
        'options' => ['id' => 'grid-pjax'],
        'columns' => [
                ['class' => 'kartik\grid\CheckboxColumn'],
            'fecha:date',
            'title',
            'descripcion:ntext',
            // 'icon',
            // 'type',
            'visto:boolean',

            
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-info btn-action remote-modal', 'target' => '_BLANK', 'data-target' => '#ajax', 'data-toggle' => 'modal']);
                    },
                ],
                'updateOptions' => [
                    'class' => 'btn btn-primary btn-action',
                ],
                'deleteOptions' => [
                    'class' => 'btn btn-danger btn-action',
                ],
                'width' => '95px',
                'noWrap' => true,
            ], 
        ],
    ]);
    ?>   
</div>


<div class="modal fade modal-scroll" id="ajax" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>



<?= $this->registerJs(' 
    $(document).ready(function(){
      $("#btn-bulk-del").click(function(){
        $.post(
            "delete-multiple", 
            {
                pk : $("#grid-pjax").yiiGridView("getSelectedRows")
            },
            function () {
                $.pjax.reload({container:"#grid-pjax"});
            }
        );
      });
    });', \yii\web\View::POS_READY);
?>