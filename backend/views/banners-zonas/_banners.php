<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\grid\GridView;
use yii\helpers\Url;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\BannersZonas */
/* @var $form yii\widgets\ActiveForm */
$model_zona = $model;
?>

<?php
$toolbar = Html::a('<i class="glyphicon glyphicon-plus"></i> Agregar ', 'javascript:void(0)', ['class' => 'btn btn-success', 'target' => '_BLANK', 'data-target' => '#banners-add-modal', 'data-toggle' => 'modal']);
?>



<div>

    <?=
    GridView::widget([
        'export' => [
            'target' => GridView::TARGET_SELF,
            'label' => 'Exportar',
        ],
        'exportConfig' => Yii::$app->params['gridview_exportConfig'],
        'dataProvider' => $banners_dataprovider,
        'toolbar' => [
                ['content' => $toolbar],
            // '{export}',
            '{toggleData}',
        ],
        'bordered' => false,
        'striped' => true,
        'condensed' => false,
        'pjax' => true,
        'panel' => [
            'heading' => '<i class="fa fa-table" aria-hidden="true"></i> Modificar Banners en  ' . $model->nombre,
            'type' => 'primary',
        ],
        'panelHeadingTemplate' => '
        <h3 class="panel-title">{heading}</h3>
        <div class="panel-actions panel-actions-keep">{toolbar}</div>
        <div class="clearfix"></div>',
        'panelBeforeTemplate' => '{before}',
        'containerOptions' => ['class' => 'grid-pjax-container'],
        'options' => ['id' => 'grid-pjax'],
        //'filterModel' => $searchModel,
        'columns' => [
            //  ['class' => 'kartik\grid\CheckboxColumn'],
                ['class' => 'kartik\grid\SerialColumn'],
            //            'id',
            'nombre',
            'activo:boolean',
                [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view}{delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-eye"></i>', ['/banners/view', 'id' => $model->id], ['class' => 'btn btn-info btn-action remote-modal', 'target' => '_BLANK', 'data-target' => '#ajax', 'data-toggle' => 'modal']);
                    },
                    'delete' => function ($url, $model, $key) use ($model_zona) {
                        return Html::a('<i class="fa fa-trash"></i>', 'javascript:void(0)', ['class' => 'btn btn-danger btn-action ajax-call btn-action-delete', 'data-params' => '', 'data-url' => Url::toRoute(['/banners-zonas/quitar-banner', 'zona' => $model_zona->id, 'banner' => $model->id]), 'data-method' => 'POST']);
                    },
                ],
                'updateOptions' => [
                    'class' => 'btn btn-primary btn-action',
                ],
                'deleteOptions' => [
                    'class' => 'btn btn-danger btn-action',
                ],
                'width' => '95px',
                'noWrap' => true,
            ],
        ],
    ]);
    ?>



</div>

<div class="modal fade modal-scroll" id="ajax" tabindex="-1" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>


<div class="modal fade modal-scroll" id="banners-add-modal" role="dialog" aria-hidden="true">      
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><strong>Agregar Banner en <?= $model->nombre ?></strong> </h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                <?= Html::dropDownList('banner-selector', null,\common\models\Banners::getArrayList(true),['id' => 'banner-selector-id','class' => 'form-control']) ?>
                </div>
            </div>
            <div class="modal-footer">
                
                <button  type="button" class="btn default" data-dismiss="modal">Cerrar</button>
                <button id="btn-banner-selector-add" type="button" class="btn btn-primary" data-dismiss="modal">Agregar</button>
            </div>
        </div>



    </div>
</div>



<?php

$addBannerUrl = Url::toRoute(['/banners-zonas/agregar-banner','zona' => $model->id]);

$this->registerJs(
        <<<JAVASCRIPT
   $('#btn-banner-selector-add').click(function(e){
        var banner_selected = $('#banner-selector-id').find(":selected").val();
        
        $.ajax({
            type: "POST",
            url: "$addBannerUrl",
            data: {banner_selected: banner_selected},
            success: function(result) {
                toastr["success"]("El banner ha sido agregado");
                $.pjax.reload({container: '#grid-pjax-pjax', timeout: 10000});
            },
            error: function(result) {

            }
        });

   });
   
   $('.btn-action-delete').bind("ajax-call-response",function(e){
        toastr["success"]("El banner ha sido removido");
        $.pjax.reload({container: '#grid-pjax-pjax', timeout: 10000});
   });
        
        
        
JAVASCRIPT
);
