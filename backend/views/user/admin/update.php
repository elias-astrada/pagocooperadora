<?php
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Url;
use yii\helpers\Html;
/**
 * @var \yii\web\View $this
 * @var \dektrium\user\models\User $user
 * @var string $content
 */
$this->title = Yii::t('user', 'Update user account');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//$this->registerCssFile(Url::to('@web') . '/css/profile.min.css');
$profile = $user->profile;
Yii::$app->params['page-header-showtitle'] = false;
?>

<?= "" // $this->render('/_alert', ['module' => Yii::$app->getModule('user')])  ?>

<?= "" //$this->render('_menu')  ?>



<div class="row">

    <!-- BEGIN PROFILE SIDEBAR -->
    <?= $this->render('admin_sidebar', compact('profile', 'user')) ?>
    <!-- END BEGIN PROFILE SIDEBAR -->
    <!-- BEGIN PROFILE CONTENT -->
    <div class="col-lg-9">
        <div class="profile-content">

            <div class="panel panel-primary">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i><?= Html::encode($this->title) ?></h3></div>

                <!-- STAT -->

                <!-- END STAT -->
                <div class="panel-body">
                    <?= $content ?>
                </div>

            </div>


        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>
