<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\widgets\Alert;
/**
 * @var yii\web\View $this
 * @var dektrium\user\models\User $user
 */

$this->title = Yii::t('user', 'Create a user account');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user'),]) ?>



<div class="panel panel-primary">
    <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-edit"></i><?= Html::encode($this->title) ?></h3></div>

    <div class="panel-body">
        <?=  Alert::widget([
            'type' => Alert::TYPE_INFO,
            'title' => 'Info!',
            'icon' => 'glyphicon glyphicon-info-sign',
            'body' => 'Las credenciales seran enviadas via email al usuario.<br/>Si quieres que la contraseña se genere automáticamente, deja el campo vacío.',
            'showSeparator' => true,
            'delay' => false,
        ]); ?>
        
        <?php $form = ActiveForm::begin([
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,

        ]); ?>

        <?= $this->render('_user', ['form' => $form, 'user' => $user]) ?>

        <div class="form-group">
   
                <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

