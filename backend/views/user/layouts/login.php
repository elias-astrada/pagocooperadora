<?php

use backend\assets\LoginAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\widgets\SideNav;

/* @var $this \yii\web\View */
/* @var $content string */

LoginAsset::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

        <script> var mainweburl = "<?= Url::to('@web'); ?>";</script>
        <script src="<?= Url::toRoute('/global/vendor/breakpoints/breakpoints.min.js', true) ?>"></script>
        <script>
            Breakpoints();
        </script>
    </head>
    <body class="animsition page-login-v2 layout-full page-dark">
        <?php $this->beginBody() ?>


        <div class="page" data-animsition-in="fade-in" data-animsition-out="fade-out">
            <div class="page-content">

                <?= $content ?>

            </div>    
        </div>

        <?php $this->endBody() ?>
        <script>
            (function (document, window, $) {
                'use strict';
                var Site = window.Site;
                $(document).ready(function () {
                    Site.run();
                });
            })(document, window, jQuery);
        </script>
    </body>
</html>
<?php $this->endPage() ?>